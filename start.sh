#!/bin/bash
echo Starting Flask-gunicorn-server for Open Machines API...
gunicorn -w 2 -b 127.0.0.1:8080 --chdir /home/jualat/api/ wsgi:app

