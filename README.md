# API documentation
## coming soon
- problem (means solution): posting project needs authentication to GitLab
- user authentication
- feature extension: update project pages, double name conflicts
## important
This project contains two branches (code and main). In main there are the files for the hugo project sites. In code you can find the app code. For deployment the main branch has to be cloned outside the code branch (hugo folder outside of the project folder).
## starting and installation - for Ubuntu minimal
### first steps
- create sudo user (here: jualat)
- change DNS-entry of the domain (here: test.jualat.de), remember www.
### preparation
1) install packages `sudo apt install python3 python3-pip nginx python3-certbot-nginx gunicorn`
2) install python packages `pip3 install flask gunicorn gitpython`
3) clone git repository for the code into the home directory: `git clone -b code https://gitlab.com/open-machines-ev/modules/project_page_api.git api`
4) clone git repository for the hugo page into the home directory: `git clone -b main https://gitlab.com/open-machines-ev/modules/project_page_api.git hugo`
5) save git credentials: `git config --global credential.helper store`, after that `git pull` and enter credentials
### services
1) create a file for the api-service: `sudo nano /lib/systemd/system/om_api.service`
2) paste the following code into it: 
```bash
[Unit]
Description=service for the open machines api
After=network.target
After=systemd-user-sessions.service
After=network-online.target

[Service]
User=jualat
Type=simple
ExecStart=/home/jualat/api/start.sh
TimeoutSec=30
Restart=on-failure
RestartSec=15
StartLimitInterval=350
StartLimitBurst=10

[Install]
WantedBy=multi-user.target
```
3) start the service `sudo systemctl start om_api`
4) enable the service `sudo systemctl enable om_api`
4) delete nginx default service: `sudo rm /etc/nginx/sites-enabled/default`
5) create file for new nginx service: `sudo nano /etc/nginx/sites-enabled/reverse-proxy.conf`
6) create the ssl certificates with certbot: `sudo certbot --nginx -d test.jualat.de -d www.test.jualat.de` (there may be an error that the certificates were downloaded, but not connected to nginx. That's okay. Just check if their location from the error message is the same as in th code in step 7)
7) paste the following code into it:
```bash
server {
    listen 80;
    listen [::]:80;
    server_name test.jualat.de;

    return 302 https://$server_name$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;
    ssl_certificate /etc/letsencrypt/live/test.jualat.de/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/test.jualat.de/privkey.pem;

    location / {
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header HOST $http_host;
                proxy_pass http://127.0.0.1:8080;
                proxy_redirect off;
    }
}
```
8) restart nginx: `sudo systemctl restart nginx`
9) often a reboot is needed: `sudo reboot`
### firewall
1) disbable firewall to prevent ssh lockout: `sudo ufw disable`
2) deny all incoming: `sudo ufw default deny incoming`
3) allow all outgoing: `sudo ufw default allow outgoing`
4) list apps: `sudo ufw app list`
5) enable apps: `sudo ufw allow 'NGINX FULL'`
6) enable apps: `sudo ufw allow 'OpenSSH'`
7) enable: `sudo ufw enable`
8) status: `sudo ufw status`
### debugging
- status/start/stop/restart of services (nginx/om_api): `sudo systemctl status om_api`
- logs of services (nginx/om_api): `journalctl -u om_api -e`
## /createProjectPage
input Parameters: title, creator, description

input content-type: JSON

Example:
``{
    'title':'your_title',
    'creator':'your_creator',
    'description':'your_project_description'
} ``

output content-type: JSON

possible outputs: "Data Received!", "Git error!", "Your input has the wrong format or content."

Example:
``{
    'message':'Data Received!'
} ``  
