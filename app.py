from flask import Flask, request
import json
from datetime import datetime
import git


app = Flask(__name__)

######################
###project-page-api###
######################

#configure GitPython
git.refresh("/usr/bin/git")
repo = git.Repo('/home/jualat/hugo/')
username = "jualat"
token = ""
#create a new remote, if it doesn't already exist
try:
    remote = repo.create_remote('main', url=f"https://@gitlab.com:open-machines-ev/modules/project_page_api.git")
except git.exc.GitCommandError:
    print('Remote already exists')

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/createProjectPage", methods=['POST'])
def createProjectPage():
    #parse JSON
    data = json.loads(request.data.decode('utf-8'))

    try:
        data['title']
        data['creator']
        data['description']
    except:
        return json.dumps({'Message': 'Your input has the wrong format or content.'})

    #create Markdown file and write content into it
    file = open(f"/home/jualat/hugo/content/posts/{data['title']}.md", "a")
    file.write("---\n")
    file.write(f"title: {data['title']}\n")
    #file.write(f"date: {datetime.now()}\n")
    file.write(f"author: {data['creator']}\n")
    file.write("draft: false\n")
    file.write("---\n")
    file.write(data['description'])
    file.close()

    #sync with the git-repository
    try:
        repo.remotes.origin.pull()  # Pull from remote repo
        if repo.is_dirty(untracked_files=True):
            diff = repo.git.diff(repo.head.commit.tree)
            repo.index.add("*")
            repo.index.commit(f"(automatic) project {data['title']} added")
            repo.remotes.origin.push() # Push changes
            message = 'Data received!'
    except Exception as e:
        message = f"Git error! {e}"

    return json.dumps({'Message': message})
